import Trampoline from '../index.mjs';

let current = 0;

new Trampoline({
    async fire() {
        console.log(current++);
        await new Promise(resolve => setTimeout(resolve, 10));
        return current === 10000;
    }
}).spring();