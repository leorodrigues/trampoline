import chai from 'chai';
import sinon from 'sinon';
import Trampoline from '../../index.mjs';

const { expect } = chai;

chai.use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const action = {
    fire: sandbox.stub(),
    handleError: sandbox.stub()
};

const subject = new Trampoline(action);

describe('Trampoline', () => {
    afterEach(() => sandbox.reset());

    describe('#sping', () => {
        it('Should spring once successfully and stop', async () => {
            action.fire.resolves(true);
            await subject.spring();
            expect(action.fire).to.be.calledOnceWithExactly();
        });
        it('Should spring once, fail due to an exception then stop', async () => {
            const error = new Error('thrown on purpose');
            action.fire.throws(error);
            action.handleError.resolves(true);
            await subject.spring();
            expect(action.fire).to.be.calledOnceWithExactly();
            expect(action.handleError).to.be.calledOnceWithExactly(error);
        });
        it('Should spring twice and successfully stop', async () => {
            action.fire.onCall(0).resolves(false);
            action.fire.onCall(1).resolves(true);
            await subject.spring();
            expect(action.fire).to.be.calledTwice
                .and.calledWithExactly()
                .and.calledWithExactly();
        });
    });
});