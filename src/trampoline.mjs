export default class Trampoline {
    constructor(action) {
        this.action = action;
    }

    async spring() {
        let stopRunning;
        try {
            stopRunning = await this.action.fire();
        } catch(error) {
            stopRunning = await this.action.handleError(error);
        } finally {
            if (!stopRunning)
                this.spring();
        }
    }
};
